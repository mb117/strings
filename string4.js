// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}


function problem4(person) {
    if ( typeof person !== 'object' || Array.isArray(person) === true) {
        return "";
    }

    let full_name = Object.keys(person).reduce(function(full_name, name) {
        name = person[name][0].toUpperCase() + person[name].substring(1).toLowerCase();

        return full_name.concat(name + " ");
    }, "");

    return full_name.trimEnd();
}

module.exports = problem4;