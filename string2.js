// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function problem2(ipAdress) {
    if ( typeof ipAdress !== 'string') {
        return [];
    }

    ipAdress = ipAdress.split(".");

    if ( ipAdress.length !== 4) {
        return [];
    }

    // every octet should be a Number and in range 0 to 255
    let result = ipAdress.every(function(octet) {
        return !(isNaN(octet) || Number(octet) < 0 || Number(octet) > 255);
    });

    return result ? ipAdress : [];
}

module.exports = problem2;