// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function problem3(date) {
    if (typeof date !== 'string') {
        return "";
    }

    date = date.split("/");

    if ( date.length !== 3) {
        return "";
    }

    // assuming date is in format dd/mm/yyyy
    let month = date[1];
    let monthName = ["January", "February", "March", "April", "May", "June", "July", "August", "September",
                    "October", "November", "December"];

    return isNaN(month) || Number(month) < 1 || Number(month) > 12 ? "" : monthName[Number(month) - 1];
}


module.exports = problem3;